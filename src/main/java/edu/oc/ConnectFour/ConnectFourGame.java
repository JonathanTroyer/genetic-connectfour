package edu.oc.ConnectFour;

import edu.oc.ConnectFour.Player.ConnectFourPlayer;

public class ConnectFourGame {
    private final ConnectFourPlayer[] players;
    private final ConnectFourBoard board;
    private int currentPlayer;

    public ConnectFourGame(ConnectFourPlayer a, ConnectFourPlayer b) {
        players = new ConnectFourPlayer[2];
        players[0] = a;
        players[0].setPlayerPiece(Piece.X);
        players[1] = b;
        players[1].setPlayerPiece(Piece.O);
        board = new ConnectFourBoard();
        currentPlayer = 0;
    }

    public ConnectFourBoard play() {
        do {
            ConnectFourPlayer player = players[currentPlayer];
            Integer move = player.getMove(board);
            board.makeMove(move, player.getPlayerPiece());
            currentPlayer = 1 - currentPlayer;
        }
        while (!board.isFull() && board.getWinPiece() == Piece.Empty);
        return board;
    }
}