package edu.oc.ConnectFour;

public enum Piece {
    X,
    O,
    Empty {
        @Override
        public String toString() {
            return "*";
        }
    };

    public Piece getOpposite() {
        if (this == Piece.X) {
            return O;
        } else if (this == Piece.O) {
            return X;
        }
        return Empty;
    }
}
