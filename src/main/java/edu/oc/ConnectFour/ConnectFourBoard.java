package edu.oc.ConnectFour;

import java.util.Arrays;

public class ConnectFourBoard {
    public static final int WIDTH = 7;
    public static final int HEIGHT = 6;
    private static final int WIN_LENGTH = 4;

    final Piece[][] board = new Piece[HEIGHT][WIDTH];
    private Piece winPiece;

    public ConnectFourBoard() {
        for (int row = 0; row < HEIGHT; row++) {
            Arrays.fill(board[row], Piece.Empty);
        }
    }

    public Piece getWinPiece() {
        return this.winPiece;
    }

    private void calcWinner(MovePos pos) {
        winPiece = board[pos.row][pos.col];

        // Check vertical (down only)
        if (pos.row <= HEIGHT - WIN_LENGTH) {
            boolean won = true;
            for (int r = pos.row; r < pos.row + WIN_LENGTH; r++) {
                if (board[r][pos.col] != winPiece) {
                    won = false;
                    break;
                }
            }
            if (won) {
                return;
            }
        }

        // Check horizontal along the move row
        int startCol = Math.max(pos.col - (WIN_LENGTH - 1), 0);
        int endCol = Math.min(pos.col + (WIN_LENGTH - 1), WIDTH - 1);
        int numConnected = 0;
        for (int c = startCol; c <= endCol; c++) {
            if (board[pos.row][c] == winPiece) {
                numConnected++;
            } else {
                numConnected = 0;
            }
        }
        if (numConnected >= WIN_LENGTH) {
            return;
        }

        // Check \ diagonal
        int startRow = Math.max(pos.row - (WIN_LENGTH - 1), 0);
        int endRow = Math.min(pos.row + (WIN_LENGTH - 1), HEIGHT - 1);

        int startLength = Math.min(pos.row - startRow, pos.col - startCol);
        startCol = pos.col - startLength;
        startRow = pos.row - startLength;

        int endLength = Math.min(endRow - pos.row, endCol - pos.col);
        endCol = pos.col + endLength;
        endRow = pos.row + endLength;

        numConnected = 0;
        int r = startRow;
        int c = startCol;
        for (; r <= endRow && c <= endCol; r++, c++) {
            if (board[r][c] == winPiece) {
                numConnected++;
            } else {
                numConnected = 0;
            }
        }
        if (numConnected >= WIN_LENGTH) {
            return;
        }

        // Check / diagonal
        startCol = Math.max(pos.col - (WIN_LENGTH - 1), 0);
        endCol = Math.min(pos.col + (WIN_LENGTH - 1), WIDTH - 1);
        startRow = Math.min(pos.row + (WIN_LENGTH - 1), HEIGHT - 1);
        endRow = Math.max(pos.row - (WIN_LENGTH - 1), 0);

        startLength = Math.min(startRow - pos.row, pos.col - startCol);
        startCol = pos.col - startLength;
        startRow = pos.row + startLength;

        endLength = Math.min(pos.row - endRow, endCol - pos.col);
        endCol = pos.col + endLength;
        endRow = pos.row - endLength;

        numConnected = 0;
        r = startRow;
        c = startCol;
        for (; r >= endRow && c <= endCol; r--, c++) {
            if (board[r][c] == winPiece) {
                numConnected++;
            } else {
                numConnected = 0;
            }
        }
        if (numConnected >= WIN_LENGTH) {
            return;
        }

        winPiece = Piece.Empty;
    }

    boolean isFull() {
        for (Piece[] col : board) {
            for (Piece p : col) {
                if (p == Piece.Empty) {
                    return false;
                }
            }
        }
        return true;
    }

    void place(MovePos pos, Piece piece) {
        board[pos.row][pos.col] = piece;
    }

    public void makeMove(int col, Piece piece) {
        int row = 0;
        while (row < HEIGHT && board[row][col] == Piece.Empty) {
            row++;
        }

        if (row != 0) {
            MovePos pos = new MovePos(row - 1, col);
            place(pos, piece);
            calcWinner(pos);
        }
    }

    public boolean isValidMove(Integer col) {
        return col != null && col >= 0 && col < WIDTH && board[0][col] == Piece.Empty;
    }

    public Piece getPiece(int row, int col) {
        return board[row][col];
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        for (Piece[] col : board) {
            for (Piece p : col) {
                str.append(p).append(" ");
            }
            str.append("\n");
        }
        return str.toString();
    }
}
