package edu.oc.ConnectFour.Player;

import edu.oc.ConnectFour.ConnectFourBoard;
import edu.oc.ConnectFour.Piece;
import edu.oc.ConnectFour.TrackingBoard;

import java.util.Random;

public class AIPlayer extends ConnectFourPlayer {
    private static final int MIN_MAX_CUTOFF = 4;
    public static final double MAX_GENOME_VAL = 10;
    public static final double MIN_GENOME_VAL = -10;
    private static final int GENOME_SIZE = 10 * (ConnectFourBoard.WIDTH * (ConnectFourBoard.HEIGHT - 3)
            + (ConnectFourBoard.WIDTH - 3) * ConnectFourBoard.HEIGHT
            + 2 * (ConnectFourBoard.WIDTH - 3) * (ConnectFourBoard.HEIGHT - 3));
    private final double[] genome = new double[GENOME_SIZE];
    private Random random;

    public AIPlayer() {
        random = new Random();
        fillRand();
    }

    public AIPlayer(long seed) {
        random = new Random(seed);
        fillRand();
    }

    public AIPlayer(double[] g) {
        // Automatically clamp values
        for (int i = 0; i < genome.length; i++) {
            genome[i] = Math.max(MIN_GENOME_VAL, Math.min(MAX_GENOME_VAL, g[i]));
        }
    }

    private void fillRand() {
        for (int i = 0; i < genome.length; i++) {
            genome[i] = random.nextDouble() * (MAX_GENOME_VAL - MIN_GENOME_VAL) + MIN_GENOME_VAL;
        }
    }

    public double[] getGenome() {
        return genome;
    }

    public Integer getMove(ConnectFourBoard board) {
        double best = Double.NEGATIVE_INFINITY;
        int move = -1;
        TrackingBoard trackingBoard = new TrackingBoard(board);
        for (int i = 0; i < ConnectFourBoard.WIDTH; i++) {
            if (!board.isValidMove(i)) {
                continue;
            }

            trackingBoard.makeMove(i, playerPiece);
            double moveVal = minMax(trackingBoard, playerPiece, 1, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
            trackingBoard.undo();

            if (moveVal >= best) {
                best = moveVal;
                move = i;
            }
        }
        return move;
    }

    private double minMax(TrackingBoard board, Piece piece, int level, double alpha, double beta) {
        Piece winPiece = board.getWinPiece();
        if (winPiece != Piece.Empty) {
            return winPiece == playerPiece ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
        }
        if (level >= MIN_MAX_CUTOFF) {
            return heuristicValue(board);
        }

        Piece movePiece = piece.getOpposite();
        double best = (piece == this.playerPiece ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
        for (int i = 0; i < ConnectFourBoard.WIDTH; i++) {
            if (!board.isValidMove(i)) {
                continue;
            }

            board.makeMove(i, movePiece);
            double moveVal = minMax(board, movePiece, level + 1, alpha, beta);
            board.undo();

            if (piece == this.playerPiece) {
                best = Math.max(best, moveVal);
                alpha = Math.max(alpha, best);
            } else {
                best = Math.min(best, moveVal);
                beta = Math.min(beta, best);
            }
            if (alpha > beta) {
                break;
            }
        }
        return best;
    }

    private double heuristicValue(ConnectFourBoard board) {
        double value = 0;
        double[] att = getAttributes(board);
        for (int i = 0; i < att.length; i++) {
            value += att[i] * genome[i];
        }
        return value;
    }

    private double[] getAttributes(ConnectFourBoard board) {
        double[] ret = new double[GENOME_SIZE];
        int a = 0;
        for (int i = 0; i < ConnectFourBoard.HEIGHT; i++) {
            for (int j = 0; j < ConnectFourBoard.WIDTH - 3; j++) {
                int xs = 0;
                int os = 0;
                for (int k = 0; k < 4; k++) {
                    if (board.getPiece(i, j + k) == Piece.X) {
                        xs++;
                    } else if (board.getPiece(i, j + k) == Piece.O) {
                        os++;
                    }
                }
                ret[a + xs] = 1.0;
                ret[a + 5 + os] = 1.0;
                a += 10;
            }
        }
        for (int i = 0; i < ConnectFourBoard.HEIGHT - 3; i++) {
            for (int j = 0; j < ConnectFourBoard.WIDTH; j++) {
                int xs = 0;
                int os = 0;
                for (int k = 0; k < 4; k++) {
                    if (board.getPiece(i + k, j) == Piece.X) {
                        xs++;
                    } else if (board.getPiece(i + k, j) == Piece.O) {
                        os++;
                    }
                }
                ret[a + xs] = 1.0;
                ret[a + 5 + os] = 1.0;
                a += 10;
            }
        }
        for (int i = 0; i < ConnectFourBoard.HEIGHT - 3; i++) {
            for (int j = 0; j < ConnectFourBoard.WIDTH - 3; j++) {
                int xs = 0;
                int os = 0;
                for (int k = 0; k < 4; k++) {
                    if (board.getPiece(i + k, j + k) == Piece.X) {
                        xs++;
                    } else if (board.getPiece(i + k, j + k) == Piece.O) {
                        os++;
                    }
                }
                ret[a + xs] = 1.0;
                ret[a + 5 + os] = 1.0;
                a += 10;
            }
        }
        for (int i = 0; i < ConnectFourBoard.HEIGHT - 3; i++) {
            for (int j = 3; j < ConnectFourBoard.WIDTH; j++) {
                int xs = 0;
                int os = 0;
                for (int k = 0; k < 4; k++) {
                    if (board.getPiece(i + k, j - k) == Piece.X) {
                        xs++;
                    } else if (board.getPiece(i + k, j - k) == Piece.O) {
                        os++;
                    }
                }
                ret[a + xs] = 1.0;
                ret[a + 5 + os] = 1.0;
                a += 10;
            }
        }
        return ret;
    }
}