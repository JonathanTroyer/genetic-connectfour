package edu.oc.ConnectFour.Player;

import edu.oc.ConnectFour.ConnectFourBoard;

import java.io.Closeable;
import java.util.Scanner;

public class HumanPlayer extends ConnectFourPlayer implements Closeable {
    private final Scanner input = new Scanner(System.in);

    public Integer getMove(ConnectFourBoard board) {
        System.out.println();
        for (int col = 1; col <= ConnectFourBoard.WIDTH; col++) {
            System.out.print(col + " ");
        }
        System.out.println();
        System.out.print(board.toString());
        System.out.print("Please enter the column for your move (1-" + ConnectFourBoard.WIDTH + "): ");
        Integer col = null;
        while (!board.isValidMove(col)) {
            try {
                col = Integer.parseInt(input.nextLine().trim()) - 1;
                if (!board.isValidMove(col)) {
                    System.out.println("That move is not valid.");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Please enter a number");
            }
        }
        return col;
    }

    @Override
    public void close() {
        input.close();
    }
}
