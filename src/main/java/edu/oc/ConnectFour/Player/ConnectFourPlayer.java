package edu.oc.ConnectFour.Player;

import edu.oc.ConnectFour.ConnectFourBoard;
import edu.oc.ConnectFour.Piece;

public abstract class ConnectFourPlayer {

    Piece playerPiece;

    public Piece getPlayerPiece() {
        return this.playerPiece;
    }

    public void setPlayerPiece(Piece playerPiece) {
        this.playerPiece = playerPiece;
    }

    public abstract Integer getMove(ConnectFourBoard board);

}