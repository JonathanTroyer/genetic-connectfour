package edu.oc.ConnectFour;

import java.util.Stack;

public class TrackingBoard extends ConnectFourBoard {

    private final Stack<MovePos> history = new Stack<>();

    public TrackingBoard(ConnectFourBoard from) {
        super();
        for (int row = 0; row < board.length; row++) {
            if (board[row].length >= 0) {
                System.arraycopy(from.board[row], 0, board[row], 0, board[row].length);
            }
        }
    }

    protected void place(MovePos pos, Piece piece) {
        super.place(pos, piece);
        history.push(pos);
    }

    public void undo() {
        if (history.size() > 0) {
            MovePos pos = history.pop();
            super.place(pos, Piece.Empty);
        }
    }
}
