package edu.oc.ConnectFour;

class MovePos {
    public final int row;
    public final int col;

    public MovePos(int row, int col) {
        this.row = row;
        this.col = col;
    }
}
