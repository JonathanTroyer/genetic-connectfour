package edu.oc;

import edu.oc.BreedingGrounds.Breeder;
import edu.oc.BreedingGrounds.Organism;
import edu.oc.ConnectFour.ConnectFourBoard;
import edu.oc.ConnectFour.ConnectFourGame;
import edu.oc.ConnectFour.Piece;
import edu.oc.ConnectFour.Player.HumanPlayer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class SimpleDriver {

    private static final String fileName = "genomes.txt";

    public static void main(String[] s) {
        // playGame();
        generate();
    }

    private static void playGame() {
        ConnectFourGame game = new ConnectFourGame(new HumanPlayer(), new HumanPlayer());
        ConnectFourBoard endState = game.play();

        System.out.print(endState.toString());
        if (endState.getWinPiece() == Piece.Empty) {
            System.out.println("Tie!");
        } else {
            System.out.println(endState.getWinPiece() + " wins!");
        }
    }

    private static void generate() {
        Breeder breeder = new Breeder(500);
        List<Organism> winners = new ArrayList<>(breeder.generate());
        winners.sort((a, b) -> b.getWins() - a.getWins());
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            for (Organism o : winners) {
                System.out.printf("%s won %d games.\n", o.getName(), o.getWins());
                for (double d : o.getPlayer().getGenome()) {
                    writer.write(d + ",");
                }
                writer.newLine();
                writer.newLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }
}