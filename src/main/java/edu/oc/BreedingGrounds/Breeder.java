package edu.oc.BreedingGrounds;

import edu.oc.ConnectFour.ConnectFourBoard;
import edu.oc.ConnectFour.ConnectFourGame;
import edu.oc.ConnectFour.Piece;
import edu.oc.ConnectFour.Player.AIPlayer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Breeder {

    // Double because math
    private final double Rounds;

    private final int MinPop = 75;
    private final int MaxPop = 125;
    private final int MinPopChange = -10;
    private final int MaxPopChange = 10;
    private final double SurvivalRate = 0.1;
    private final double MinMutationChance = 0.01;
    private final double RewriteChance = 0.02;
    private int PopSize = (MaxPop - MinPop) / 2 + MinPop;
    private double MutationChance = MinMutationChance;

    private Set<Organism> population = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private int LastPopSize = PopSize;

    public Breeder(double rounds) {
        Rounds = rounds;
        for (int i = 0; i < PopSize; i++) {
            population.add(new Organism(new AIPlayer()));
        }
    }

    public Collection<Organism> generate() {
        for (int i = 0; i < Rounds; i++) {
            fill(population);
            LastPopSize = PopSize;
            System.out.format("Round %d. %d will survive of %d.\n", i + 1, numToKeep(), population.size());
            playAll();
            cull();

            if (i < Rounds - 1) {
                List<Organism> popList = new ArrayList<>(population);
                popList.sort((a, b) -> b.getWins() - a.getWins());
                int count = 0;
                for (Organism o : popList) {
                    if (count < popList.size() - 1) {
                        System.out.print(o.getName() + " won " + o.getWins() + " games, ");
                    } else {
                        System.out.print("and " + o.getName() + " won " + o.getWins() + " games.");
                    }
                    count++;
                }
                System.out.println();
            }

            // Update the next population size and mutation rate
            PopSize += ThreadLocalRandom.current().nextInt(MaxPopChange - MinPopChange) + MinPopChange;
            PopSize = Math.max(MinPop, PopSize);
            PopSize = Math.min(MaxPop, PopSize);

            MutationChance += (-Math.log(i / Rounds + 1) + 1) / (Rounds / 3);
        }
        return population;
    }

    private int numToKeep() {
        return (int) (PopSize * SurvivalRate) + 1;
    }

    private void playAll() {
        for (Organism o : population) {
            o.wins = 0;
        }
        population.parallelStream().forEach(o -> {
            for (Organism o2 : population) {
                if (o == o2) {
                    continue;
                }

                ConnectFourGame game = new ConnectFourGame(o.getPlayer(), o2.getPlayer());
                ConnectFourBoard endState = game.play();
                if (endState.getWinPiece() == Piece.Empty) {
                    continue;
                }
                if (endState.getWinPiece() == o.getPlayer().getPlayerPiece()) {
                    o.wins++;
                } else {
                    o2.wins++;
                }
            }
        });
    }

    private void fill(Collection<Organism> pool) {
        int poolSize = pool.size();
        CopyOnWriteArrayList<Organism> safePool = new CopyOnWriteArrayList<>(pool);
        double gamesPerOrganism = (LastPopSize - 1) * 2;
        IntStream.range(poolSize, PopSize).parallel().forEach(i -> {
            Organism strong = safePool.get(ThreadLocalRandom.current().nextInt(poolSize));
            Organism weak = safePool.get(ThreadLocalRandom.current().nextInt(poolSize));
            if (weak.wins > strong.wins) {
                Organism tmp = strong;
                strong = weak;
                weak = tmp;
            }
            AIPlayer player = breed(strong, weak, gamesPerOrganism);
            Organism o = new Organism(player, strong.getSurname());
            while (population.contains(o)) {
                player = breed(strong, weak, gamesPerOrganism);
                o = new Organism(player, strong.getSurname());
                // Ensure we have a chance to add new genes to the pool
                if (ThreadLocalRandom.current().nextDouble() <= MutationChance) {
                    if (ThreadLocalRandom.current().nextDouble() <= RewriteChance) {
                        player = new AIPlayer();
                        o = new Organism(player);
                    }
                }
            }
            population.add(o);
        });
    }

    private void cull() {
        List<Organism> popList = new ArrayList<>(population);
        popList.sort((o1, o2) -> o2.getWins() - o1.getWins());
        population = Collections.newSetFromMap(new ConcurrentHashMap<>());
        population.addAll(popList.subList(0, numToKeep()));
    }

    private AIPlayer breed(Organism strong, Organism weak, double gamesPerOrganism) {
        double[] newGenome = strong.getPlayer().getGenome().clone();
        double[] weakGenome = weak.getPlayer().getGenome().clone();
        double copyChance = weak.getWins() / gamesPerOrganism;
        int genomeChunkSize = 5;
        for (int i = 0; i < newGenome.length; i += genomeChunkSize) {
            if (ThreadLocalRandom.current().nextDouble() > copyChance) {
                continue;
            }
            if (i + genomeChunkSize - i >= 0) {
                System.arraycopy(weakGenome, i, newGenome, i, i + genomeChunkSize - i);
            }
        }
        if (ThreadLocalRandom.current().nextDouble() <= MutationChance) {
            mutate(newGenome);
            if (ThreadLocalRandom.current().nextDouble() <= MutationChance) {
                MutationChance = MinMutationChance;
            }
        }
        return new AIPlayer(newGenome);
    }

    private void mutate(double[] genome) {
        for (int i = 0; i < genome.length; i++) {
            if (ThreadLocalRandom.current().nextDouble() > MutationChance) {
                continue;
            }
            if (ThreadLocalRandom.current().nextDouble() <= RewriteChance) {
                genome[i] = ThreadLocalRandom.current().nextDouble()
                        * (AIPlayer.MAX_GENOME_VAL - AIPlayer.MIN_GENOME_VAL) + AIPlayer.MIN_GENOME_VAL;
            } else {
                genome[i] = genome[i] * (ThreadLocalRandom.current().nextDouble() * (1.2 - 0.8)) + 0.8;
            }

        }
    }
}
