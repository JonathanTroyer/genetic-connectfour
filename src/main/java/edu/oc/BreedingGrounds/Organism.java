package edu.oc.BreedingGrounds;

import edu.oc.ConnectFour.Player.AIPlayer;

import java.util.Arrays;
import java.util.Random;

public class Organism {
    int wins = 0;
    private final AIPlayer player;
    private final String properName;
    private final int hash;
    // Names compiled using Dwarf, Goliath, Half-Orc, and Human names from D&D 5E
    private final String[] ProperNames = {"Adrik", "Alberich", "Alethra", "Amafrey", "Amber", "Ander", "Anton", "Aoth",
            "Arizima", "Artin", "Arveene", "Aseir", "Atala", "Audhild", "Aukan", "Baern", "Baggi", "Balama", "Bardeid",
            "Bardryn", "Barendd", "Bareris", "Betha", "Blath", "Bor", "Borivik", "Bran", "Brottor", "Bruenor", "Cefrey",
            "Ceidil", "Chathi", "Chien", "Dagnal", "Dain", "Darrak", "Darvin", "Delg", "Dench", "Diero", "Diesa",
            "Dona", "Dorn", "Eberk", "Eglath", "Ehput-Ki", "Einkil", "Eldeth", "Emen", "Engong", "Esvele", "Evendur",
            "Faila", "Falkrunn", "Fargrim", "Faurgar", "Feng", "Finellen", "Flint", "Fodel", "Frath", "Fyevarra",
            "Gae-Al", "Gardain", "Gauthak", "Gell", "Geth", "Glar", "Gorstag", "Grigor", "Grim", "Gunnloda", "Gurdis",
            "Hama", "Harbek", "Haseid", "Helja", "Helm", "Henk", "Hlin", "Holg", "Huang", "Hulmarra", "Igan", "Ilde",
            "Ilikan", "Immith", "Imsh", "Imzel", "Ivor", "Jalana", "Jandar", "Jasmal", "Jhessail", "Kanithar", "Kansif",
            "Kao", "Kara", "Katernin", "Kathra", "Keothi", "Kerri", "Keth", "Kethoth", "Kethra", "Khemed", "Kildrak",
            "Kosef", "Kristryd", "Krusk", "Kung", "Kuori", "Lander", "Lao", "Liftrasa", "Ling", "Lo-Kag", "Luisa",
            "Lureene", "Luth", "Madislak", "Malark", "Malcer", "Manneo", "Mara", "Marcon", "Mardred", "Marta",
            "Maveith", "Mehmen", "Mei", "Meilil", "Mhurren", "Miri", "Mival", "Morgran", "Morn", "Mumed", "Murithi",
            "Myev", "Nalla", "Natali", "Navarra", "Neega", "Nephis", "Nulara", "Olga", "Olma", "Orel", "Orilo", "Orsik",
            "Oskar", "Ovak", "Ownka", "Paavu", "Pavel", "Pethani", "Pieron", "Pin", "Quara", "Ralmevik", "Ramas",
            "Randal", "Rangrim", "Rimardo", "Riswynn", "Romero", "Ront", "Rowan", "Rurik", "Salazar", "Sannl", "Sefris",
            "Seipora", "Selise", "Sergor", "Shandri", "Shaumar", "Shautha", "Shevarra", "Shin", "Shump", "Silifrey",
            "So-Kehur", "Stedd", "Stor", "Sudeiman", "Sum", "Sutha", "Taklinn", "Taman", "Tammith", "Tan", "Tana",
            "Tessele", "Thalai", "Thazar-De", "Thokk", "Thola", "Thoradin", "Thorin", "Thotham", "Torbera", "Tordek",
            "Torgga", "Traubon", "Travok", "Ulfgar", "Umara", "Umbero", "Urhur", "Urth", "Uthal", "Vaunea", "Veit",
            "Vimak", "Vistra", "Vladislak", "Vola", "Volen", "Vonda", "Vondal", "Wan", "Westra", "Yasheira", "Yevelda",
            "Yuldra", "Zasheida", "Zasheir", "Zolis", "Zora" };
    private final String[] Surnames = {"Agosto", "Amblecrown", "An", "Ankhalab", "Anskuld", "Astorio", "Bai", "Balderk",
            "Basha", "Battlehammer", "Bearkiller", "Bersk", "Brawnanvil", "Brightwood", "Buckman", "Calabra", "Chao",
            "Chen", "Chergoba", "Chernin", "Chi", "Dankil", "Dawncaller", "Domine", "Dotsk", "Dumein", "Dundragon",
            "Dyernina", "Evenwood", "Fai", "Falone", "Fearless", "Fezim", "Fireforge", "Flintfinder", "Frostbeard",
            "Gorunn", "Greycastle", "Hahpet", "Helder", "Holderhek", "Horncarver", "Hornraven", "Iltazyara", "Ironfist",
            "Jassan", "Jia", "Jiang", "Jun", "Keeneye", "Khalid", "Kulenov", "Lackman", "Lei", "Lian", "Loderr",
            "Lonehunter", "Long", "Longleaper", "Lutgehr", "Marivaldi", "Marsk", "Mei", "Meng", "Mostana",
            "Murnyethara", "Nathandem", "Nemetsk", "On", "Pashar", "Pisacar", "Qiao", "Ramondo", "Rein", "Rootsmasher",
            "Rumnaheim", "Sepret", "Shan", "Shemov", "Shui", "Skywatcher", "Starag", "Stayanoga", "Steadyhand",
            "Stormwind", "Strakeln", "Tai", "Tallstag", "Threadtwister", "Torunn", "Twice-Orphaned", "Twistedlimb",
            "Ulmokina", "Ungart", "Uuthrakt", "Wen", "Windrivver", "Wordpainter" };
    private String surname;

    public Organism(AIPlayer player) {
        this.player = player;
        this.hash = Arrays.hashCode(player.getGenome());
        Random nameGen = new Random(hash);
        this.properName = ProperNames[nameGen.nextInt(ProperNames.length)];
        this.surname = Surnames[nameGen.nextInt(Surnames.length)];
    }

    public Organism(AIPlayer player, String surname) {
        this(player);
        this.surname = surname;
    }

    public AIPlayer getPlayer() {
        return player;
    }

    public int getWins() {
        return wins;
    }

    public String getName() {
        return properName + " " + surname;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Organism) {
            return Arrays.equals(this.getPlayer().getGenome(), ((Organism) other).getPlayer().getGenome());
        }
        return false;
    }
}